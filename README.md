Installation
============

Deploy to e.g. /var/www/$wiki/localextensions/COsMeetings

Enable in config.php with:

wfLoadExtension("COsMeetings", "/var/www/$wikidir/localextensions/COsMeetings/extension.json");

(N.B. $wikidir can remain literal there; it's a mediawiki PHP variable)

Usage
=====

* Assign an action with a list of due dates
  - {{ #action: Assignee | Do something | 2021-01-13,2021-01-20 }}

* Mark an action as done. N.B. the third argument to actiondone is ignored but will usually be there when we convert an action to an actiondone
  - {{ #actiondone: Assignee | Do something | 2021-01-13,2021-01-20 }}

* Star an item until a given date
  - {{ #staruntil: 2021-01-13 }}

* Automatically produce a friendly page title, deduced from the date in the page name
  - {{ #nicemeetingtitle: }}

* Automatically link to previous minutes, assuming the meeting was 7 days ago
  - ```[[{{ #previousminuteslink: }}]]```

* Automatically produce next meeting date, assuming it will be 7 days hence
  - {{ #nextmeetingdate: }}

However, because we used to do all this via native mediawiki templating, you should
make sure to define Templates that wrap each of the parser hooks we're now providing

* Template:Action
  - {{ #action: {{{1}}} | {{{2}}} | {{{3}}} }}

* Template:Actiondone
  - {{ #actiondone: {{{1}}} | {{{2}}} | {{{3}}} }}

* Template:Staruntil
  - {{ #staruntil: {{{1}}} }}
