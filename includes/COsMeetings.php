<?php

class COsMeetings {
  public static function onParserFirstCallInit( Parser $parser ) {
    $parser->setFunctionHook( 'action', [ self::class, 'cosMeetingsAction' ] );
    $parser->setFunctionHook( 'actiondone', [ self::class, 'cosMeetingsActionDone' ] );
    $parser->setFunctionHook( 'staruntil', [ self::class, 'cosMeetingsStarUntil' ] );
    $parser->setFunctionHook( 'nicemeetingtitle', [ self::class, 'cosMeetingsNiceMeetingTitle' ] );
    $parser->setFunctionHook( 'previousminuteslink', [ self::class, 'cosPreviousMinutesLink' ] );
    $parser->setFunctionHook( 'nextmeetingdate', [ self::class, 'cosNextMinutesDate' ] );
  }

  public static function cosMeetingsAction(Parser $parser, $param1, $param2, $param3 = '') {
    # support two forms of argument list, matching either the current preferred format of:
    # {{#action: ALT | write some code | 1999-09-01 }}
    # or the legacy version of:
    # {{#action: ALT: write some code | 1999-09-01 }}

    if($param3) {
      $assignee = $param1;
      $action = $param2;
      $dates = $param3;
    } else {
      $action = $param1;
      $dates = $param2;
    }

    $title = $parser->Title();

    # NB underscores in titles have been converted to spaces by this point
    $parts = explode(" ", $title);
    $page_date = strtotime($parts[3] . "-" . $parts[2] ."-" . $parts[1]);

    $bf_count = substr_count($dates, ",");

    $due_dates = explode(",", $dates);
    $due_date = end($due_dates);

    $msg = ': ';

    # embolden due actions
    if($page_date >= strtotime($due_date)) {
      $msg .= "'''Action''' ";
    } else {
      $msg .= "Action ";
    }

    # support both two-argument and three-argument methods of calling
    if($assignee) {
      $msg .= "'''$assignee''': $action ";
    } else {
      $msg .= "$action ";
    }

    $msg .= "Due: ";

    if($bf_count>0) {
      $msg .= "(bf $bf_count) ";
    }

    $msg .= $due_date;
    return $msg;
  }

  public static function cosMeetingsActionDone(Parser $parser, $param1, $param2, $param3 = '') {
    return "<s>ACTION:</s> DONE $param1: $param2 &#10004;";
  }

  public static function cosMeetingsStarUntil(Parser $parser, $param1) {
    $title = $parser->Title();

   # NB underscores in titles have been converted to spaces by this point
    $parts = explode(" ", $title);
    $page_date = strtotime($parts[3] . "-" . $parts[2] ."-" . $parts[1]);

    $msg = "";

    $star_until = strtotime($param1);
    if($page_date < $star_until) {
      $msg = "(☆) ";
    }

    return $msg;
  }

  public static function cosMeetingsNiceMeetingTitle(Parser $parser) {
    $title = $parser->Title();
    $parts = explode(" ", $title);
    $page_date = strtotime($parts[3] . "-" . $parts[2] ."-" . $parts[1]);

    return $parts[0] . " —  " . date("l jS F Y", $page_date);
  }

  public static function cosPreviousMinutesLink(Parser $parser) {
    $title = $parser->Title();
    $parts = explode(" ", $title);
    $page_date = $parts[3] . "-" . $parts[2] ."-" . $parts[1];

    $last_week = date("Y_m_d", strtotime("$page_date - 1 week"));

    return "Minutes $last_week";
  }

  public static function cosNextMinutesDate(Parser $parser) {
    $title = $parser->Title();
    $parts = explode(" ", $title);
    $page_date = $parts[3] . "-" . $parts[2] ."-" . $parts[1];

    $next = date("l jS F Y", strtotime("$page_date + 1 week"));

    return $next;
  }
}
